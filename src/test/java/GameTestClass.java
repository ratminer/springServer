import com.google.gson.Gson;
import endlessOnline.model.Player;
import endlessOnline.model.User;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by home on 07/11/2017.
 */
public class GameTestClass {
    private static final int SERVER_PORT = 32211;
    private static final String SERVER_ADDRESS = "localhost";

    private TestClient testClient;

    User testUser = new User()
            .name("testuser")
            .password("password");

    @Before
    public void setup() {
        testClient = new TestClient(SERVER_ADDRESS, SERVER_PORT);
    }

    @Test
    public void testMove() throws Exception {

        Player character = new Player()
                .name("hello")
                .id(2)
                .user(testUser);

        String json = testClient.callFunction("move", new Gson().toJson(character));
    }

}
