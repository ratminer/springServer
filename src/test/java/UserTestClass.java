import com.google.gson.Gson;
import endlessOnline.model.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ResourceBundle;

/**
 * Created by home on 24/10/2017.
 */

public class UserTestClass {

    private static final int SERVER_PORT = 32211;
    private static final String SERVER_ADDRESS = "localhost";

    private TestClient testClient;
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("MessageData");

    User testUser = new User()
            .name("testuser")
            .password("password");

    private Gson gson = new Gson();

    @Before
    public void setup() {
        testClient = new TestClient(SERVER_ADDRESS, SERVER_PORT);
    }

    @Test
    public void testRegister() throws Exception {
        String json = testClient.callFunction("register", testUser.toJson());
        System.out.println(json);
        Assert.assertTrue(json.equals(resourceBundle.getString("register_successful")));
    }

    @Test
    public void testLogin() throws Exception {
        String json = testClient.callFunction("login", testUser.toJson());
        System.out.println(json);
        Assert.assertTrue(json.equals(resourceBundle.getString("user_online")));
    }

    @Test
    public void testLogOut() throws Exception {
        String json = testClient.callFunction("logout", testUser.toJson());
        System.out.println(json);
        Assert.assertTrue(json.equals(resourceBundle.getString("user_offline")));
    }

    @After
    public void tearDown() {
        testClient.close();
    }

}
