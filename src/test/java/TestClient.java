import org.springframework.integration.ip.tcp.connection.MessageConvertingTcpMessageMapper;
import org.springframework.integration.ip.tcp.connection.TcpMessageMapper;
import org.springframework.integration.ip.tcp.serializer.MapJsonSerializer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.support.converter.MapMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import server.Headers;

import javax.net.SocketFactory;
import java.net.Socket;
import java.util.Map;

/**
 * Created by home on 24/10/2017.
 */
public class TestClient {

    private Socket socket;

    private MapMessageConverter converter;
    private TcpMessageMapper messageMapper;
    private MapJsonSerializer serializer;

    private MessageHeaders headers;

    public TestClient(String serverString, int serverPort) {
        try {
            socket = SocketFactory.getDefault().createSocket(serverString, serverPort);
        } catch (Exception e) {
            e.printStackTrace();
        }

        converter = new MapMessageConverter();
        converter.setHeaderNames(Headers.method, Headers.status);

        messageMapper = new MessageConvertingTcpMessageMapper(converter);
        serializer = new MapJsonSerializer();

    }

    public String callFunction(String methodName, String json) throws Exception {
        send(MessageBuilder.withPayload(json)
                .setHeader(Headers.method, methodName)
                .build());

        Message response = receive();
        if (Headers.isError(response)) {
            throw new Exception((String) response.getPayload());
        }
        return (String) response.getPayload();
    }

    public void send(Message msg) throws Exception {
        serializer.serialize((Map<?, ?>) messageMapper.fromMessage(msg), socket.getOutputStream());
    }

    public Message receive() throws Exception{
        Message response = null;
        try {
            response = converter.toMessage(serializer.deserialize(socket.getInputStream()), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public void close() {
        try {
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
