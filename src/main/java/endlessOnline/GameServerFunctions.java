package endlessOnline;

import com.google.gson.Gson;
import endlessOnline.model.Player;
import endlessOnline.model.PlayerRepository;
import endlessOnline.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.Message;
import server.Server;
import server.ServerFunctions;

import java.util.function.Function;

/**
 * Created by home on 07/11/2017.
 */
@Configuration
@Profile("game")
public class GameServerFunctions extends ServerFunctions {

    @Autowired
    public PlayerRepository playerRepository;

    @Autowired
    public UserRepository userRepository;

    @Bean
    public Function<String, Message> move() {
        return json -> {

            Player character = new Gson().fromJson(json, Player.class);

            Player dbA = playerRepository.findById(character.id()).get();

            dbA.x(dbA.x() + 1);
            dbA.y(dbA.y() + 1);

            playerRepository.save(dbA);

            return Server.response("updated");
        };
    }

}
