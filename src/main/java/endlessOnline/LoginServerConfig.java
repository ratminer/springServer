package endlessOnline;

import endlessOnline.model.ServerInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import server.LogicServer;

/**
 * Created by home on 07/11/2017.
 */
@Configuration
@Profile("login")
public class LoginServerConfig extends LogicServer{

    @Value("${host}")
    private String host;

    @Value("${port}")
    private int port;

    @Bean
    public ServerInfo serverInfo() {
        return new ServerInfo(this);
    }

    @Override
    public int port() {
        return port;
    }

    @Override
    public String host() {
        return host;
    }

    @Override
    public String type() {
        return ServerTypes.login;
    }

}
