package endlessOnline.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by home on 24/10/2017.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    User findByName(String name);

}
