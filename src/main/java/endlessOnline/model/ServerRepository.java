package endlessOnline.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;


/**
 * Created by home on 31/10/2017.
 */
public interface ServerRepository extends CrudRepository<ServerInfo, Long> {
    List<ServerInfo> findByType(String type);
}
