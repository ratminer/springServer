package endlessOnline.model;

import lombok.Data;
import lombok.experimental.Accessors;
import server.Server;
import server.ServerConfig;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 * Created by home on 31/10/2017.
 */
@Data
@Entity
public class ServerInfo {

    @Id
    private String hostPort;

    private String type;

    public ServerInfo(){}

    public ServerInfo(ServerConfig config) {
        hostPort = config.hostPort();
        type = config.type();
    }

}