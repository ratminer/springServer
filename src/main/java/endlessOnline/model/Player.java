package endlessOnline.model;

import com.fasterxml.jackson.annotation.JsonView;
import l.o.l.Exclude;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * Created by home on 01/11/2017.
 */
@Entity
@Accessors(fluent = true)
@Data
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name = "";

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Exclude
    private User user;

    private int x = 0;
    private int y = 0;

}

