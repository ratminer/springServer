package endlessOnline.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import l.o.l.Exclude;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

/**
 * Created by home on 19/10/2017.
 */
@Data
@Accessors(fluent = true)
@Entity
public class User {

    private static ObjectMapper objectMapper = new ObjectMapper();
    private static Gson gson = new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getAnnotation(Exclude.class) != null;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    }).create();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String name = "";
    private String password = "";

    private String email = "";

    @ManyToOne
    @JoinColumn(name = "host_port")
    private ServerInfo serverInfo;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Player> players;


    public static User fromJson(String json) {
        return gson.fromJson(json, User.class);
    }

    public String toJson() {
        return gson.toJson(this);
    }

    public boolean isOnline() {
        return serverInfo != null;
    }
}