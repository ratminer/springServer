package endlessOnline.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by home on 01/11/2017.
 */
public interface PlayerRepository extends CrudRepository<Player, Long> {
}
