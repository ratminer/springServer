package endlessOnline;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import endlessOnline.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.Message;
import server.ServerFunctions;

import java.util.ResourceBundle;
import java.util.function.Function;

import static l.o.l.B.Return;
import static server.Server.error;
import static server.Server.response;

/**
 * Created by home on 27/10/2017.
 */
@Configuration
@Profile("login")
public class LoginFunctions extends ServerFunctions {

    private static ResourceBundle myBundle = ResourceBundle.getBundle("MessageData");

    @Autowired
    public PlayerRepository playerRepository;

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public ServerInfo serverInfo;

    @Bean
    public Function<String, Message> createCharacter() {
        return json -> {
            User user = User.fromJson(json);
            User dbUser = userRepository.findByName(user.name());
            playerRepository.save(new Player().user(dbUser));
            return response(userRepository.save(dbUser).toJson());
        };
    }

    @Bean
    public Function<String, Message> login() {
        return json -> {
            User user = User.fromJson(json);
            System.out.println("Username: " + user.name() + " Password: " + user.password());
            User dbUser = userRepository.findByName(user.name());

            return (dbUser == null)? error(myBundle.getString("invalid_username"))
                    : (dbUser.isOnline())? error(myBundle.getString("user_online"))
                    : (!dbUser.password().equals(user.password()))? error(myBundle.getString("invalid_password"))
                    : response(userRepository.save(dbUser.serverInfo(serverInfo)).toJson());
                    /*: Return(response(myBundle.getString("user_online")),
                        () -> userRepository.save(dbUser.serverInfo(serverInfo)));*/
        };
    }

    @Bean
    public Function<String, Message> logout() {
        return json -> {
            User user = User.fromJson(json);
            System.out.println("Username: " + user.name() + " Password: " + user.password());
            User dbUser = userRepository.findByName(user.name());

            return (dbUser == null)? error(myBundle.getString("invalid_username"))
                    : (!dbUser.isOnline())? error(myBundle.getString("user_offline"))
                    : (!dbUser.password().equals(user.password()))? error(myBundle.getString("invalid_password"))
                    : Return(response(myBundle.getString("user_offline")),
                        () -> userRepository.save(dbUser.serverInfo(null)));
        };
    }

    @Bean
    public Function<String, Message> register() {
        return json -> {
            User user = User.fromJson(json);
            System.out.println("Username: " + user.name() + " Password: " + user.password());
            User dbUser = userRepository.findByName(user.name());

            return (dbUser != null) ? error(myBundle.getString("user_exists"))
                    : Return(response(myBundle.getString("register_successful")),
                        () -> userRepository.save(user));
        };
    }
}