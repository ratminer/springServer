package endlessOnline;

import endlessOnline.model.PlayerRepository;
import endlessOnline.model.ServerInfo;
import endlessOnline.model.ServerRepository;
import endlessOnline.model.UserRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.support.PropertiesBuilder;

import java.net.InetAddress;

/**
 * Created by home on 07/11/2017.
 */
@SpringBootApplication
public class Application {

    @Autowired
    private ServerRepository serverRepository;

    @Autowired
    public PlayerRepository playerRepository;

    @Autowired
    public UserRepository userRepository;

    public static void main(String[] args) throws Exception {

/*        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter server type " + Arrays.toString(ServerTypes.values()));

        String serverType = scanner.next();
        System.out.println(serverType);*/

        ConfigurableApplicationContext context = new SpringApplicationBuilder(Application.class)
                .properties(new PropertiesBuilder()
                        .put("host", InetAddress.getLocalHost().getHostAddress())
                        .put("port", 32211)
                        .get())
                //.profiles(serverType)
                .profiles("login")
                .run(args);
    }

    @Bean
    public InitializingBean initializingBean(ServerInfo serverInfo) {
        return () -> {
/*
            User newUser = new User()
                    .name("testuser")
                    .password("password");

            Character character = new Character().name("hello")
                    .user(newUser);

            userRepository.save(newUser);
            characterRepository.save(character);
*/

            serverRepository.save(serverInfo);
        };
    }

}
