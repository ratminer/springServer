package endlessOnline;

/**
 * Created by home on 07/11/2017.
 */
public interface ServerTypes {
    String login = "login";
    String game = "game";

    static String[] values() {
        return new String[]{login, game};
    }
}
