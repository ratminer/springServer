package server;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.handler.ServiceActivatingHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;

import java.util.Map;
import java.util.function.Function;

/**
 * Created by home on 01/11/2017.
 */
@Configuration
public abstract class LogicServer extends Server {

    @Autowired
    public Map<String, Function<String, Message>> functions;

    @Bean
    public MessageHandler messageHandler() {
        return new ServiceActivatingHandler(message -> {
            String methodName = (String) message.getHeaders().get(Headers.method);
            String json = (String) message.getPayload();
            JsonElement jsonElement = new JsonParser().parse(json);

            return tcpHeader(message,
                    (!functions.containsKey(methodName)) ? error("invalid method " + methodName)
                            : (jsonElement.isJsonObject() || jsonElement.isJsonArray()) ? functions.get(methodName).apply(json)
                            : error("invalid payload")
            );
        });
    }
}
