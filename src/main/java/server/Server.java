package server;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.ip.IpHeaders;
import org.springframework.integration.ip.tcp.TcpReceivingChannelAdapter;
import org.springframework.integration.ip.tcp.TcpSendingMessageHandler;
import org.springframework.integration.ip.tcp.connection.MessageConvertingTcpMessageMapper;
import org.springframework.integration.ip.tcp.connection.TcpNetServerConnectionFactory;
import org.springframework.integration.ip.tcp.serializer.MapJsonSerializer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.support.converter.MapMessageConverter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;

/**
 * Created by home on 26/10/2017.
 */
@Configuration
public abstract class Server implements ServerConfig {

    // server connection factory
    @Bean
    public TcpNetServerConnectionFactory serverConnectionFactory() {
        TcpNetServerConnectionFactory tcf = new TcpNetServerConnectionFactory(port());

        MapMessageConverter converter = new MapMessageConverter();
        converter.setHeaderNames(Headers.method, Headers.status);
        tcf.setMapper(new MessageConvertingTcpMessageMapper(converter));

        MapJsonSerializer serializer = new MapJsonSerializer();
        tcf.setSerializer(serializer);
        tcf.setDeserializer(serializer);

        return tcf;
    }

    // message processing flow
    @Bean
    public IntegrationFlow flow(TcpNetServerConnectionFactory tcf, MessageHandler messageHandler) {

        TcpReceivingChannelAdapter serverInputNode = new TcpReceivingChannelAdapter();
        serverInputNode.setConnectionFactory(tcf);

        TcpSendingMessageHandler serverOutputNode = new TcpSendingMessageHandler();
        serverOutputNode.setConnectionFactory(tcf);

        return IntegrationFlows.from(serverInputNode)
                .handle(messageHandler)
                .handle(serverOutputNode)
                .get();
    }

    // static message builder functions
    public static Message response(String string) {
        return MessageBuilder.withPayload(string)
                .setHeader(Headers.method, Headers.response)
                .setHeader(Headers.status, Headers.Status.success)
                .build();
    }

    public static Message error(String string) {
        return MessageBuilder.withPayload(string)
                .setHeader(Headers.method, Headers.response)
                .setHeader(Headers.status, Headers.Status.error)
                .build();
    }

    public static Message tcpHeader(Message source, Message message) {
        return MessageBuilder.fromMessage(message)
                .setHeader(IpHeaders.CONNECTION_ID, source.getHeaders().get(IpHeaders.CONNECTION_ID))
                .build();
    }
}
