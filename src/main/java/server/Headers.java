package server;

import org.springframework.messaging.Message;

/**
 * Created by home on 28/10/2017.
 */
public interface Headers {
    String method = "method";
    String status = "status";
    String response = "response";

    interface Status {
        String request = "request";
        String error = "error";
        String success = "success";
    }

    static boolean isError(Message message) {
        return message.getHeaders().get(status).equals(Status.error);
    }

}
