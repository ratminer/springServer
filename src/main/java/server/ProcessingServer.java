package server;

import endlessOnline.model.ServerInfo;
import endlessOnline.model.ServerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.handler.ServiceActivatingHandler;
import org.springframework.integration.ip.tcp.connection.TcpNetClientConnectionFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by home on 01/11/2017.
 */
public abstract class ProcessingServer extends Server {

    @Autowired
    public ServerRepository serverRepository;

    @Bean
    public Map<String, List<ServerInfo>> serverMap() {
        Map<String, List<ServerInfo>> serverMap = new HashMap<>();

        Arrays.asList("login", "logout", "register")
            .stream().forEach(s -> serverMap.put(s, serverRepository.findByType("login")));

        return serverMap;
    }

    @Bean
    public MessageHandler messageHandler() {
        return new ServiceActivatingHandler(message -> {
            String methodName = (String) message.getHeaders().get(Headers.method);

            Message response = null;

            if (!serverMap().containsKey(methodName)) response = error("invalid methodName");

            String[] server = serverMap().get(methodName).get(0).getHostPort().split(":");

            return tcpHeader(message, response);
        });
    }

}
