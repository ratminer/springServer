package server;

import org.springframework.messaging.MessageHandler;

/**
 * Created by home on 07/11/2017.
 */
public interface ServerConfig {

    int port();
    String host();

    String type();

    public default String hostPort() {
        return host() + ":" + port();
    }

    MessageHandler messageHandler();
}
