package l.o.l;

import org.springframework.data.util.Pair;
import org.springframework.messaging.Message;

import java.util.function.Function;

/**
 * Created by home on 29/10/2017.
 */
public interface B {

    static <R> R Return(R val, Runnable r) {
        r.run();
        return val;
    }
}
